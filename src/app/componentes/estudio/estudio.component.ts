import { Component, Input, Output, OnInit, EventEmitter, ViewChild } from '@angular/core';
import { DtoEstudio } from "../estudio/dtoEstudio";
import { CornerstoneDirective } from '../../directives/cornerstone.directive';
import { environment } from 'src/environments/environment';
import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';

@Component({
  selector: 'app-estudio',
  templateUrl: './estudio.component.html',
  styleUrls: ['./estudio.component.css']
})
export class EstudioComponent implements OnInit {

  @Input() dtoEstudio:DtoEstudio;
  @Output() actualizarDTO = new EventEmitter<DtoEstudio>(); 

  @ViewChild(CornerstoneDirective) vc: CornerstoneDirective;

  panelactivo:number = 1;
  tocken:string="";
  nombrepaciente:string="";  
  estudio:any = [];
  estudio_nombre:string="";
  isMostrarbtn:boolean=true;

  // ---------- Visualizador ---------
  invertir: boolean = false;
  herramienta_visor: any;
  rotado: boolean = false;
  innerWidth: any;
  visor_height: number = 0;
  contenido_height_inicial: number = 0;
  contenido_height: number = 500;
  imageData: any;
  estructura: any;
  estudio_seleccionado: any = [];
  estudio_seleccionado_tamanio: any;
  urlVisor = "";
  hash = ""; 
  baseUrl: any;
  imagenes: any = [];
  mostrarImagen = false;
  numeroimg: number = 0; 

  constructor( private http: HttpClient,) { 
    this.dtoEstudio = new DtoEstudio(); 
  }

  ngOnInit(): void {
    this._cargainfoComponente();
    this.contenido_height_inicial = window.innerHeight;
    this.contenido_height = this.contenido_height_inicial - 75;
    this.visor_height = this.contenido_height - 0;
    this.innerWidth = window.innerWidth;
    this.urlVisor = environment.endpoint.visor+'visor';
  }

//============= recibo info de principal ===================================
  _cargainfoComponente(){
    this.tocken = this.dtoEstudio.tocken;
    if(this.tocken != ""){
      this.nombrepaciente = this.dtoEstudio.nombrepaciente;    
      this.estudio = this.dtoEstudio.estudio;    
      this.estudio_nombre = this.estudio.estudio;   
      this.hash = this.estudio.hash;
      this.numeroimg = this.dtoEstudio.numeroimg;
      this.abrirVisualizador();
    }else{
      this._activarpanelestudio(3);
    }
  }

  verHD() {
    if (this.vc.cargando == false)
      this.vc.cargarDicom(this.estudio, this.hash, this.tocken);
  }

  async abrirVisualizador() {

    this.estructura = await this.http.get(environment.endpoint.visor + 
         'visor/get-estructura?token=' + this.tocken + "&hash=" + this.hash).toPromise();
    this.mostrarImagen = true;
    this.vc.cargarDicom(this.estructura, this.tocken, this.estudio);
    this._cargarImagenVisualizador(this.numeroimg);

  }

  _cargarImagenVisualizador(i: number) {
    var temp = this.estructura.estructura[i].imgs;
    var series = [];
    temp.forEach(element => {
      series.push(element.imagen)
    });
    this.baseUrl = environment.endpoint.visor + 'visor/img-get-jpg?token=' + this.tocken + "&hash=" + this.hash + '&img=';
    this.imagenes = series.map(seriesImage => `${this.baseUrl}${seriesImage}`);
    this.estudio_seleccionado_tamanio = series.length;

    this.vc.cargarDicom2(this.imagenes);
  }

  setear_herramienta(herramienta: string) {
    if (herramienta == 'invert') {
      if (this.invertir == false) {
        this.invertir = true;
        this.herramienta_visor = herramienta;
      } else {
        this.invertir = false;
        this.herramienta_visor = "retirarInvertir";
      }
    } else if (herramienta == 'rotate') {
      if (this.rotado == false) {
        this.rotado = true;
        this.herramienta_visor = herramienta;
      } else {
        this.rotado = false;
        this.herramienta_visor = "rotate2";
      }
    } else {
      this.herramienta_visor = herramienta;
    }
    
  }

  //============= envio info a detalle Estudio ===================================
  _activarpanelestudio(cadena:any){
    this.dtoEstudio.numeropanel = cadena;
    this._actualizarDtoPrincipal(this.dtoEstudio);
  }

  _actualizarDtoPrincipal(populate:DtoEstudio): void {   
    this.actualizarDTO.emit(populate);
  }

}

import { AfterViewInit, Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { ReCaptchaV3Service } from 'ngx-captcha';
import { JsonPipe } from '@angular/common';
import { DomSanitizer } from '@angular/platform-browser';
PNotify.defaults.styling = 'bootstrap4'; // Bootstrap version 4
PNotify.defaults.icons = 'fontawesome4';
import PNotify from 'pnotify/dist/es/PNotify';
import { environment } from 'src/environments/environment';
import { LogService } from 'src/app/services/log.service';
import { ParamServiceService } from 'src/app/services/param-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  token_google_api:string="";
  token_google_respuesta:string="";
  logo:any="";
  imagePath="";
  nroestudio:string=""; //= 265758;
  codigo:any = ""; //= 'IF7GEG78';
  institucion_name:string = "";
  captcha = false;
  hoy = new Date;
  errorInstitucion = false;
  botonIngresar = false;
  cargandoCaptcha = true;
  cargandoInstitucion = true;

  public siteKey?: string = '6LdCo5sUAAAAAKqBfZaBD4FeO8D7W5hTQEMvYXKC';
  constructor(
    private router: Router,
    private http: HttpClient,
    private reCaptchaV3Service: ReCaptchaV3Service,
    private _sanitizer: DomSanitizer,
    private log: LogService,
    private paramServiceService: ParamServiceService   
  ) {

  }

  ngOnInit() {
    this.token_google_api = "6LdCo5sUAAAAAKqBfZaBD4FeO8D7W5hTQEMvYXKC";
    this.nroestudio = environment.login.nroturno;
    this.codigo = environment.login.codigo;
    console.log(window.location.hostname);
    console.log("este es el nro" + this.nroestudio)
    //this.nroestudio = '';
    //this.codigo = '';
    //this.institucion_name = this.getParamValueQueryString("institucion");
    this.institucion_name = 'biobox';
    //localStorage.setItem('institucion', this.institucion_name);
    //localStorage.setItem('token_bb', '');
    //localStorage.setItem('hash', '');
    this.traerLogo(this.institucion_name);
    this.reCaptchaV3Service.execute(this.siteKey, 'login', (token) => {
      this.token_google_respuesta = token;
      this.captcha = true;
      this.cargandoCaptcha = false;
      console.log("Captcha cargado!");
    });

    this.log.enviar('login','carga',this.institucion_name,200);

    console.log("institucion name: ",this.institucion_name);
  }

  getParamValueQueryString( paramName: string ) {
    const url = window.location.href;
    let paramValue;
    if (url.includes('?')) {
      const httpParams = new HttpParams({ fromString: url.split('?')[1] });
      paramValue = httpParams.get(paramName);
    }
    return paramValue;
  }


  async traerLogo(institucion: string) {
    var logo = await this.http.get(environment.endpoint.visor + 'visor/institucion-info?name=' +institucion).toPromise();
    console.log(logo);
    this.imagePath = logo['logo'];
    if(logo['code']==200){
      this.log.enviar('login','institucion-info',this.institucion_name,logo['code']);
      this.errorInstitucion = false;
    }
    else {
      this.log.enviar('login','institucion-info',this.institucion_name,logo['code'], logo['msg']);
      this.errorInstitucion = true;
    }
    this.cargandoInstitucion = false;
    console.log("institucion cargada!");
  }

  ingresar() {
    if(!this.nroestudio || !this.codigo){
      PNotify.error({
        text: "Ingrese nro de estudio y código para ingresar",
        delay: 3000
      });      
      this.log.enviar('login','ingreso',this.institucion_name,300,'Faltan datos');
    }else{
      var body = {
        "name": this.institucion_name,
        "token_google": this.token_google_respuesta,
        "nroturno": this.nroestudio,
        "codigo": this.codigo
      }
      var url = environment.endpoint.visor+"visor/login-json"+
        "?name="+this.institucion_name+
        "&token_google="+this.token_google_respuesta+
        "&nroturno="+this.nroestudio+
        "&codigo="+this.codigo.toUpperCase();
        
      var httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json'
        })
      };
  
      this.http.get(url).subscribe(
        data => {
          if(data["ok"] == true){
            localStorage.setItem('token_bb', data["token"]);
            //localStorage.setItem('institucion', this.institucion_name);
            //this.log.enviar('login','ingreso',this.institucion_name,200);
            //this.router.navigate(['/principal'], { queryParams: { "token": data['token'] } });
            this.router.navigate(['/principal']);

          } else {
            this.log.enviar('login','ingreso',this.institucion_name,301, data["msg"]);
              
            this.captcha = false;
            this.reCaptchaV3Service.execute(this.siteKey, 'login', (token) => {
              this.token_google_respuesta = token;
              this.captcha = true;
            });
            
            if(data['errorCode']==500){
              // recargamos el captcha
            } else {
              PNotify.error({
                text: data['msg'],
                delay: 3000
              });                
            }
          }
        },
        error => {
          console.log("Error", error);
          this.log.enviar('login','carga',this.institucion_name,300, 'error de carga');
        }
      );
      //this.router.navigate(['/home']);
    }
  }

}

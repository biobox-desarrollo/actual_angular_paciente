import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DetalleestudioComponent } from './detalleestudio.component';

describe('DetalleestudioComponent', () => {
  let component: DetalleestudioComponent;
  let fixture: ComponentFixture<DetalleestudioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DetalleestudioComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DetalleestudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

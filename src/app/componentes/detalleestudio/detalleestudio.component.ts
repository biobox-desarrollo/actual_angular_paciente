import { Component, Input, Output, OnInit, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import {DtoDetalleEstudio} from "./dtoDetalleEstudio";
PNotify.defaults.styling = 'bootstrap4'; // Bootstrap version 4
PNotify.defaults.icons = 'fontawesome4';
import PNotify from 'pnotify/dist/es/PNotify';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LogService } from 'src/app/services/log.service';

@Component({
  selector: 'app-detalleestudio',
  templateUrl: './detalleestudio.component.html',
  styleUrls: ['./detalleestudio.component.css']
})
export class DetalleestudioComponent implements OnInit {

  @Input() dtoDetalleEstudio:DtoDetalleEstudio;
  @Output() actualizarDTO = new EventEmitter<DtoDetalleEstudio>(); 

  panelactivo:number = 1;
  tocken:string="";

  //DATOS JSON
  paciente:string = "";
  modalidad:string = "";
  estudio_nombre:string = "";
  fecha:string = "";
  disponible_web: number = 0;
  disponible_mega: number = 0;
  estudio: any = [];
  estudios: any = [];
  recursos: any = [];
  informe_pdf: any;
  
  // información interna
  imagePath = ""; 
  institucion_name = "";
  //token_bb = "";
  turno = "";
  hash = "";
  vence = 0;
  registerForm: FormGroup;
  

  //------ Carga Serie Imagenes ----------
  estructura: any;
  serieestudio: any = [];
  ver = 'inicio';
  otrasseries: any = [];
  baseUrl: any;
  
  constructor(private router: Router,
    private log: LogService,
    private http: HttpClient,) { 
    this.dtoDetalleEstudio = new DtoDetalleEstudio(); 
  }

  ngOnInit(): void {
    this._cargainfoComponente();
  }

  //============= recibo info de principal ===================================
  _cargainfoComponente(){
    this.tocken = this.dtoDetalleEstudio.tocken;
    if(this.dtoDetalleEstudio.numturno != ""){
      this.turno = this.dtoDetalleEstudio.numturno;
    }
    this.traerInfo();
    this.traerDatos();
  }

  async traerInfo() {

    let info = await this.http.get(environment.endpoint.visor + 'visor/info-token?token=' +this.tocken).toPromise();
  
    if(info){
      this.imagePath = info['logo'];
      this.institucion_name = info['name'];
      if(this.turno == ""){
        this.turno = info['turno'];
      }
      this.vence = info['vence'];      
    } else {
      this.router.navigate(['/login']);
    }
  }

  async traerDatos() {

    this.estudios = await this.http.get(environment.endpoint.visor + 'visor/get-estudios?token=' + this.tocken).toPromise();
    let arrayestudio = (this.estudios.lista).filter(data => data.turno == this.turno);
    //let arrayestudio = Array.from(this.estudios.lista).filter(data => data['turno'] == this.turno);
    this.estudio =  arrayestudio[0];
    if(this.estudio.recursos.length > 0){
       this.recursos = this.estudio.recursos[0];
    }

    this.hash = this.estudio.hash;    
    const dataUsuario = await this.http.get(environment.endpoint.visor + 'visor/get-usuario?token=' + this.tocken).toPromise();
    this.paciente = dataUsuario["usuario"]["nombre"];
    this.modalidad = this.estudio["modalidad"];
    this.estudio_nombre = this.estudio["estudio"];
    this.disponible_mega = this.estudio["disponible_mega"];
    this.disponible_web = this.estudio["disponible_web"];
    this.fecha = this.estudio["fecha"];
    this.cargarserieimagenes();
  }

  async cargarserieimagenes() {

    this.estructura = await this.http.get(environment.endpoint.visor + 'visor/get-estructura?token=' + this.tocken + "&hash=" + this.hash).toPromise();
    this.ver = "visor";
    var temp = this.estructura.estructura[0].imgs;
    var series = [];
    temp.forEach(element => {
      series.push(element.imagen)
    });

    this.baseUrl = environment.endpoint.visor + 'visor/img-get-jpg?token=' + this.tocken + "&hash=" + this.hash + '&img=';
    var temp2 = this.estructura.estructura;
    this.otrasseries = [];
    temp2.forEach(element => {
      this.otrasseries.push(element);
    });

  }

  informe() {
    this.informe_pdf = environment.endpoint.visor + 'visor/get-informe?token=' + this.tocken + "&hash=" + this.hash;
    window.open(this.informe_pdf, '_blank');
    
  }

  //============= envio info a principal ===================================
  _activarpanelestudio(cadena:any,i: number){
    this.dtoDetalleEstudio.numeropanel = cadena;
    this.dtoDetalleEstudio.nombrepaciente=this.paciente;    
    if(cadena == 1){
      this.dtoDetalleEstudio.estudio = [];
      this.dtoDetalleEstudio.numeroimg = 0;    
    }else{
      this.dtoDetalleEstudio.estudio = this.estudio;
      this.dtoDetalleEstudio.numeroimg = i;    
    }
    this._actualizarDtoPrincipal(this.dtoDetalleEstudio);
  }

  _actualizarDtoPrincipal(populate:DtoDetalleEstudio): void {   
    this.actualizarDTO.emit(populate);
  }

}

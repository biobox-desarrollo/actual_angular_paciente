import { ComponentFixture, TestBed } from '@angular/core/testing';

import { InformesListComponent } from './informes-list.component';

describe('InformesListComponent', () => {
  let component: InformesListComponent;
  let fixture: ComponentFixture<InformesListComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ InformesListComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(InformesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

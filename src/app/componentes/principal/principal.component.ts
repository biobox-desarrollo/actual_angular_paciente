import { Component, OnInit } from '@angular/core';
import { Router} from '@angular/router';
import { DtoPrincipal } from "../principal/dtoPrincipal";
import { DtoEstudioList } from "../estudio-list/dtoEstudioList";
import { DtoDetalleEstudio } from "../detalleestudio/dtoDetalleEstudio";
import { DtoEstudio } from "../estudio/dtoEstudio";
// import { ParamServiceService } from 'src/app/services/param-service.service';

@Component({
  selector: 'app-principal',
  templateUrl: './principal.component.html',
  styleUrls: ['./principal.component.css']
})
export class PrincipalComponent implements OnInit {

  //public dtoPrincipal:DtoPrincipal;
  public dtoEstudioList:DtoEstudioList;
  public dtoDetalleEstudio:DtoDetalleEstudio;
  public dtoEstudio:DtoEstudio;
  public tocken:string="";
  public urlTree:any="";

  panelactivo:number = 3;

  constructor(private router: Router) {
    this.dtoEstudioList = new DtoEstudioList();
    this.dtoDetalleEstudio = new DtoDetalleEstudio(); 
    this.dtoEstudio = new DtoEstudio(); 
    //this.dtoPrincipal = new DtoPrincipal();
    // this.urlTree = this.router.parseUrl(this.router.url);
    // this.tocken = this.urlTree.queryParams['token'];

  }

  ngOnInit(): void {

    // if(this.tocken == ""){
    //   let estadotoken = localStorage.getItem('token_bb');
    //   if(estadotoken != ""){
    //     this.tocken = estadotoken;
    //     this._cargainfoDetalleEstudio();
    //   }else{
    //     //localStorage.setItem('token_bb', "");
    //     localStorage.removeItem('token_bb');
    //     this.router.navigate(['/login']);
    //   }
    // }

    this.tocken = localStorage.getItem('token_bb');
    if(this.tocken != ""){
      this._cargainfoDetalleEstudio();
    }else{
      this.router.navigate(['/login']);
    }

  }

  //========= Lista de estudio ======================
  //////========= envio info Lista estudio ======================
  _cargaEstudioList(){    
    this.dtoEstudioList.tocken = this.tocken;
  }
  //////========= recibo info Lista estudio ======================
  _paramEstudioList(populate:DtoEstudioList): void {
    this.dtoDetalleEstudio.numturno = populate.numturno;
    this._cargainfoDetalleEstudio();
    this.panelactivo = populate.numeropanel;
  }

  //========= detalle estudio ======================
  //////========= envio info detalle estudio ======================
  _cargainfoDetalleEstudio(){    
    this.dtoDetalleEstudio.tocken = this.tocken;
  }
  //////========= recibo info detalle estudio ======================
  _paramDetalleEstudio(populate:DtoDetalleEstudio): void {
    if(populate.numeropanel == 4){
      this.dtoEstudio.tocken = this.tocken;
      this.dtoEstudio.nombrepaciente=populate.nombrepaciente;      
      this.dtoEstudio.estudio = populate.estudio;
      this.dtoEstudio.numeroimg = populate.numeroimg;
      //this._cargainfoEstudio();
    }else if(populate.numeropanel == 1){
      this._cargaEstudioList();
    }
    this.panelactivo = populate.numeropanel;
  }

  //========= estudio ======================
  //////========= envio info Estudio ======================
  _cargainfoEstudio(){    
    this.dtoEstudio.tocken = this.tocken;
  }
  //////========= recibo info Estudio ======================
  _paramEstudio(populate:DtoEstudio): void {
    this.panelactivo = populate.numeropanel;
  }

}

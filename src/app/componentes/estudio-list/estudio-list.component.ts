import { Component,Input, Output, OnInit,EventEmitter, HostListener, ChangeDetectorRef, ViewChild, ContentChild, ElementRef, TemplateRef, SimpleChanges, ViewContainerRef } from '@angular/core';
//import { CornerstoneService } from '../../services/cornerstone.service';
// import { PlatformLocation } from "@angular/common";
// import { Router } from '@angular/router';
import { HttpClient, HttpHeaders, HttpParams  } from '@angular/common/http';
// import { CornerstoneDirective } from '../../directives/cornerstone.directive';
// import * as moment from 'moment';
PNotify.defaults.styling = 'bootstrap4'; // Bootstrap version 4
PNotify.defaults.icons = 'fontawesome4';
import PNotify from 'pnotify/dist/es/PNotify';
//import { FormBuilder, FormGroup, Validators } from '@angular/forms';
//import * as $ from 'jquery';
import { environment } from 'src/environments/environment';
//import { LogService } from 'src/app/services/log.service';
import {DtoEstudioList } from "./dtoEstudioList";

@Component({
  selector: 'app-estudio-list',
  templateUrl: './estudio-list.component.html',
  styleUrls: ['./estudio-list.component.css']
})
export class EstudioListComponent implements OnInit {

  @Input() dtoEstudioList:DtoEstudioList;
  @Output() actualizarDTO = new EventEmitter<DtoEstudioList>(); 
  
  tocken:string="";

  //DATOS JSON
  paciente: String = "";
  estudios: any = [];
  cargando: boolean = false;
  //registerForm: FormGroup;
  esmovil: boolean = false;
  imagePath = ''; 
  institucion_name = '';
  

  constructor(
    //private ref: ChangeDetectorRef,
    //location: PlatformLocation,
    //private router: Router,
    private http: HttpClient
    //private formBuilder: FormBuilder
  ) {
    // location.onPopState(() => {
    //   alert(window.location);
    // });
  }

  ngOnInit() {
    this.esmovil = false;
    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)){
      this.esmovil = true;
    }
    // this.registerForm = this.formBuilder.group({
    //   email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$')]],
    // });
    
    //$(".grecaptcha-badge").css("display", "none");
    this._cargainfoComponente();
  }

  //get f() { return this.registerForm.controls; }

  _cargainfoComponente(){
    this.tocken = this.dtoEstudioList.tocken;
    if(this.tocken != ""){
      this.traerInfo();
      this.traerDatos();
    }else{
      //this.router.navigate(['/login']);
      this._activarpanelestudio(3,"");
    }
    
  }

  async traerInfo() {
     var info = await this.http.get(environment.endpoint.visor + 'visor/info-token?token=' +this.tocken).toPromise();
     if(info){
       this.imagePath = info['logo'];
     } else {
       //this.router.navigate(['/login']);
       this._activarpanelestudio(3,"");
     }
  }

  async traerDatos() {
    this.estudios = await this.http.get(environment.endpoint.visor + 'visor/get-estudios?token=' + this.tocken).toPromise();
    const dataUsuario = await this.http.get(environment.endpoint.visor + 'visor/get-usuario?token=' + this.tocken).toPromise();
    this.paciente = dataUsuario["usuario"]["nombre"];
  }

//============= envio info a principal ===================================
  _activarpanelestudio(cadena:any, cadena2:any){
    this.dtoEstudioList.numeropanel = cadena;
    this.dtoEstudioList.numturno = cadena2
    this._actualizarDtoPrincipal(this.dtoEstudioList);
  }

  _actualizarDtoPrincipal(populate:DtoEstudioList): void {   
    this.actualizarDTO.emit(populate);
  }

}

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app.routing';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms'; 
import { ReactiveFormsModule } from '@angular/forms';
import {CornerstoneDirective} from "./directives/cornerstone.directive";

// import {MatToolbarModule} from '@angular/material/toolbar'; 
// import {MatIconModule} from '@angular/material/icon';
// import {MatButtonModule} from '@angular/material/button';

import { PrincipalComponent } from './componentes/principal/principal.component';
import { LoginComponent } from './componentes/login/login.component';
import { EstudioComponent } from './componentes/estudio/estudio.component';
import { EstudioListComponent } from './componentes/estudio-list/estudio-list.component';
import { InformesListComponent } from './componentes/informes-list/informes-list.component';
import { DetalleestudioComponent } from './componentes/detalleestudio/detalleestudio.component';

//import { RECAPTCHA_V3_SITE_KEY, RecaptchaV3Module } from 'ng-recaptcha';
import { NgxCaptchaModule } from 'ngx-captcha';
import { PdfViewerModule } from 'ng2-pdf-viewer';

@NgModule({
  declarations: [
    AppComponent,
    PrincipalComponent,
    LoginComponent,
    EstudioComponent,
    EstudioListComponent,
    InformesListComponent,
    DetalleestudioComponent,
    CornerstoneDirective
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    // MatToolbarModule,
    // MatIconModule,
    // MatButtonModule
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgxCaptchaModule,
    PdfViewerModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

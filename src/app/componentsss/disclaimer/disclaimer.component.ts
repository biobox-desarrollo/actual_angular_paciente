import {Component,Input, ViewChild} from '@angular/core';
import { BsModalService, BsModalRef, ModalDirective  } from "ngx-bootstrap/modal";

@Component({
    selector: 'common-modal',
    templateUrl: './disclaimer.component.html'
  })

  export class CommonModalComponent {
    @ViewChild('childModal') public childModal:ModalDirective;
    @Input() title?:string;
   constructor() {
   }
   show(){
     this.childModal.show();
   }
   hide(){
     this.childModal.hide();
   }
 }
 
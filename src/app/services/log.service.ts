import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class LogService {

  constructor(
    private http: HttpClient) {
      console.log('Servicio de Log, listo!');
  }

  enviar(controller:string, action:string, iname:string, codigo: number,
    mensaje?: string, token?: string, hash?: string, turno?: string, 
    recurso?: number, extra?: string){

    let url = environment.endpoint.log + 
    'log?c=' + controller +
    '&a=' + action +
    '&i=' + iname +
    '&o=' + codigo;
    
    url+= mensaje?  '&m='+mensaje : '';
    url+= token?    '&t='+token : '';
    url+= hash?     '&e='+hash : '';
    url+= turno?    '&u='+turno : '';
    url+= recurso?  '&r='+recurso : '';
    url+= extra?    '&x='+extra : '';

    if(environment.logEnabled)
      this.http.get(url).toPromise();
  }

}

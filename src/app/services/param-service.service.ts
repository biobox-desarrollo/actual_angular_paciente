import { EventEmitter, Injectable, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ParamServiceService {
  @Output() userkey: EventEmitter<any> = new EventEmitter(); 
  constructor() { }
}

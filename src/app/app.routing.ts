import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoginComponent } from './componentes/login/login.component';
import { PrincipalComponent} from './componentes/principal/principal.component';
import { EstudioListComponent } from './componentes/estudio-list/estudio-list.component';
import { EstudioComponent } from './componentes/estudio/estudio.component';
import { InformesListComponent } from './componentes/informes-list/informes-list.component';



const routes: Routes = [

  {
    path: '',
    component: LoginComponent
  },

  {
    path: 'login',
    component: LoginComponent
  },

  {
    path: 'principal',
    component: PrincipalComponent
  }, 

  {
    path: '**',
    component: LoginComponent
  },

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

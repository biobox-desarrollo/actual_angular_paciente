//import { HomeComponent } from './../components/home/home.component';
import { EstudioComponent } from './../componentes/estudio/estudio.component';
import { Directive, ElementRef, Input, AfterViewInit, HostListener, ViewChild } from '@angular/core';
import * as $ from 'jquery';
import * as cornerstone from 'cornerstone-core';
import * as cornerstoneMath from 'cornerstone-math';
import * as cornerstoneTools from 'cornerstone-tools';
import * as cornerstoneWebImageLoader from 'cornerstone-web-image-loader';
import * as Hammer from 'hammerjs';
import { environment } from 'src/environments/environment';

cornerstoneTools.external.$ = $;
cornerstoneTools.external.Hammer = Hammer;
cornerstoneTools.external.cornerstone = cornerstone;
cornerstoneWebImageLoader.external.cornerstone = cornerstone;

var stack2;
var imageIds2 = [];
var loadProgress;
@Directive({
  selector: '[cornerstone]',
})

export class CornerstoneDirective implements AfterViewInit {


  ngAfterViewInit(): void {

  }

  element: any;
  imageList = [];
  imageIds2Temp = [];
  currentIndex = 0;
  series: any;

  imagenCargada: boolean = false;
  cargando: boolean = false;
  cargandoC: any;
  primer_estudio = [];
  imageIds: any;

  stack: any;
  invertido: boolean = false;
  loadProgress: any = {
    "imageIds": [],
    "total": 0,
    "remaining": 0,
    "percentLoaded": 0,
  };
  token_bb: any;
  estudio: any;

  public innerWidth: any;
  //esmovil: boolean = false;
  listacargada = [];
  @Input('image')
  set image(imageData: any) {
    if (imageData) {
      if (!this.imageList.filter(img => img.imageId === imageData.imageId).length) {
        this.imageList.push(imageData);
      }
      if (imageData.imageId) {
      }
      //console.log("cambios");
    }
  }

  @Input('data')
  set data(estudio_seleccionado: any) {
    this.series = estudio_seleccionado;
  }

  constructor(
    public elementRef: ElementRef
  ) {
    this.elementRef = elementRef;
  }

  ngOnInit() {

    //$("#cargandoImagenes").addClass("oculto"); 
    let s = document.createElement('script');
    s.type = 'text/javascript';
    s.innerText = 'cornerstoneWebImageLoader.external.cornerstone = cornerstone;';
    this.elementRef.nativeElement.appendChild(s);
    cornerstoneWebImageLoader.external.cornerstone = cornerstone;

    this.element = this.elementRef.nativeElement;
    cornerstone.enable(this.element);
    cornerstone.resize(this.element, true);
    //console.log('iniciando');
    this.element.addEventListener('cornerstoneimagerendered', this.onImageRendered);
    this.element.addEventListener('cornerstonenewimage', this.onNewImage);
    cornerstone.events.addEventListener("cornerstoneimageloaded", this.onImageLoaded);
    //this.token_bb = localStorage.getItem('token_bb');
    //this.hash = localStorage.getItem('hash');
    this.innerWidth = window.innerWidth;
    if (this.innerWidth < 430) {
      //this.esmovil = true;
      $("#stack").addClass('botonActivado');
      $("#stackspan").addClass('spanActivado');
    } else {
      //this.esmovil = false;
      $("#brillo").addClass('botonActivado');
      $("#brillospan").addClass('spanActivado');
    }
  }

  cargarDicom(serie: any, token: string, estudio: any) {
    //$("#dicomWrapper").hide();
    this.token_bb = token;
    this.estudio = estudio;

    $("#dicomWrapper canvas").css('display', 'none');
    var series = [];
    var temp = serie.estructura[0].imgs;
    var series = [];

    temp.forEach(element => {
      series.push(element.imagen)
    });

    var baseUrl = environment.endpoint.visor + 'visor/img-get-jpg?token=' + this.token_bb + "&hash=" + this.estudio.hash + '&img=';

    imageIds2 = series.map(
      seriesImage => `${baseUrl}${seriesImage}`
    );
    
    $("#cargandoImagenes").removeClass("oculto");   
    $('#imageNumAndCount').text("Imagen 1/"+imageIds2.length);
    $('#zoomText').text(" ");
    $('#tips').text(' ');
    //$("#cargandoImagenes").removeClass("oculto");
    stack2 = {
      currentImageIdIndex: 0,
      imageIds: imageIds2
    }

    loadProgress = {
      "imageIds": stack2.imageIds.slice(0),
      "total": stack2.imageIds.length,
      "remaining": stack2.imageIds.length,
      "percentLoaded": 0,
    };

    this.listacargada = [];
    this.cargaDemorada(imageIds2, 0);
  }

  cargaDemorada(imageIds: any[], i: number) {  

    if (typeof imageIds[i] === 'undefined') {
      $("#cargandoImagenes").addClass("oculto");      
      return true
    } else {
      if(i==0)
        this.cargando = true;     
      try {
        //console.log(i, imageIds.length, imageIds[i]);
        cornerstone.loadAndCacheImage(imageIds[i]).then((image) => {
          this.listacargada.push(image);
          let total = imageIds.length-1;
          this.cargando = i != total;
          console.log('cargando ... '+i+' de '+total);
          if(i==0){
            this.mostrarImagenes();
          }
          this.cargaDemorada(imageIds, ++i)
        });
      } catch (e) {
        //$("#cargandoImagenes").addClass("oculto");      
      }
    }
    /*if (i >= imageIds.length) {
    
      setTimeout(() => {
        this.mostrarImagenes();
      }, 2500);
    } */
  }


  mostrarImagenes() {
    cornerstone.enable(this.element);
    cornerstone.displayImage(this.element, this.listacargada[0]);
    //$("#dicomImage").children('canvas').eq(1).css('display','none')
    //setTimeout( () => window.dispatchEvent(new Event('resize')), 500);
    setTimeout( () => cornerstone.resize(this.element, true), 500);

    cornerstoneTools.mouseInput.enable(this.element);
    cornerstoneTools.mouseWheelInput.enable(this.element);
    cornerstoneTools.stackScrollWheel.activate(this.element);

    cornerstoneTools.touchInput.enable(this.element);
    //cornerstoneTools.zoomTouchDrag.activate(this.element);
    //cornerstoneTools.zoomTouchPinch.activate(this.element);
    cornerstoneTools.stackScrollTouchDrag.activate(this.element);
    
    cornerstoneTools.addStackStateManager(this.element, ['stack', 'playClip']);
    cornerstoneTools.addToolState(this.element, 'stack', stack2);
    cornerstoneTools.addTimeSeriesStateManager(this.element, ['timeSeries', 'timeSeriesPlayer', 'timeSeriesScroll']);
    cornerstoneTools.probeTool4D.activate(this.element, 1);
    cornerstoneTools.wwwc.activate(this.element, 1);
    //cornerstoneTools.wwwcTouchDrag.activate(this.element); 
    //console.log('cargando imágenes',$("#cargandoImagenes").css('display'));
    
    $("#dicomWrapper canvas").css('display', 'none');
    $("#dicomWrapper canvas").first().css('display', 'block');
  }
 
  cargarDicom2(imagenes: any[]) {

    //$("#cargandoImagenes").removeClass("oculto");
    this.disableAllTools();
    
    imageIds2 = imagenes;

    $("#dicomWrapper canvas").css('display', 'none');

    $("#cargandoImagenes").removeClass("oculto");   
    $('#imageNumAndCount').text("Imagen 1/"+imageIds2.length);
    $('#zoomText').text(" ");
    $('#tips').text(' ');

    stack2 = {
      currentImageIdIndex: 0,
      imageIds: imageIds2
    }
    
    loadProgress = {
      "imageIds": stack2.imageIds.slice(0),
      "total": stack2.imageIds.length,
      "remaining": stack2.imageIds.length,
      "percentLoaded": 0,
    };

    //cornerstoneTools.mouseInput.enable(this.element);
    //cornerstoneTools.mouseWheelInput.enable(this.element);
    this.listacargada = [];
    this.cargaDemorada(imageIds2, 0);

  }

  

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    cornerstone.resize(this.element, true);
    this.innerWidth = window.innerWidth;
    /*if (this.innerWidth < 430) {
      this.esmovil = true;
    } else {
      this.esmovil = false;
    }
    console.log("es movil: "+this.esmovil);*/
  }

  onNewImage(e) {
    $("#imageNumAndCount").text("Imagen " + (stack2.currentImageIdIndex + 1) + "/" + imageIds2.length);

    //$('#dicomWrapper canvas:hidden').remove();
    //$("#cargandoImagenes").addClass("oculto"); 
  }

  onImageLoadProgress(e) {
    //console.log("aaaaa", e);
  }

  onImageRendered(e) {

    var eventData = e.detail;
    var viewport = eventData.viewport;
    //$('#mrbottomleft').text("WW/WC: " + Math.round(viewport.voi.windowWidth) + "/" + Math.round(viewport.voi.windowCenter));
    $('#zoomText').text("Zoom: " + viewport.scale.toFixed(2));
    
  };

  onImageLoaded(event) {

    var eventData = event.detail;
    /*var imageId = eventData.image.imageId;

    
    var imageIds = loadProgress.imageIds;*/
    var imageId = eventData.image.imageId;
    var imageIds = loadProgress["imageIds"];


    // Remove all instances, in case the stack repeats imageIds
    for (var i = imageIds.length - 1; i >= 0; i--) {
      if (imageIds[i] === imageId) {
        imageIds.splice(i, 1);
        //$("#cargandoImagenes").addClass("oculto");
      }
    }

    // Populate the load progress object
    loadProgress.remaining = imageIds.length;

    //loadProgress["percentLoaded"] = parseInt(100 - (loadProgress['remaining'] / loadProgress['total'] * 100, 10));

    if (loadProgress['remaining'] != loadProgress['total']) {

      //$("#cargandoImagenes").addClass("oculto");
    }

  }

  @Input('herramienta')
  set tools(tools: any) {

    if (tools == "rotate") {
      /*
      cornerstoneTools.rotate.activate(this.element, 1);
      cornerstoneTools.rotateTouchDrag.activate(this.element);
      */
      this.disableAllTools();
      const viewport = cornerstone.getViewport(this.element);
      viewport.rotation -= 90;
      cornerstone.setViewport(this.element, viewport);
      $('#tips').text('Tips: Al hacer click en la herramienta rota a 45 grados hacia la derecha');

    }

    if (tools == "wwwc") {

      this.disableAllTools();
      cornerstoneTools.wwwc.activate(this.element, 1);
      cornerstoneTools.wwwcTouchDrag.activate(this.element);
      $('#tips').text('Tips: click + desplazamiento se cambia el brillo de la imagen');
    }

    if (tools == "zoom") {

      this.disableAllTools();
      cornerstoneTools.zoom.activate(this.element, 1);
      cornerstoneTools.zoomTouchDrag.activate(this.element);
      cornerstoneTools.zoomTouchPinch.activate(this.element);
      $('#tips').text('Tips: click + desplazamiento hacia arriba o abajo para hacer zoom a la imagen');
    }

    if (tools == "pan") {

      this.disableAllTools();
      cornerstoneTools.pan.activate(this.element, 1);
      cornerstoneTools.panTouchDrag.activate(this.element);
      $('#tips').text('Tips: click + desplazamiento mueve la imagen en el plano');
    }

    if (tools == "magnify") {

      this.disableAllTools();
      cornerstoneTools.magnify.activate(this.element, 1);
      cornerstoneTools.magnifyTouchDrag.activate(this.element);

    }

    if (tools == "invert") {
      this.disableAllTools();
      const viewport = cornerstone.getViewport(this.element);
      viewport.invert = !viewport.invert;
      cornerstone.setViewport(this.element, viewport);
      $('#tips').text('Tips: Al hacer click en la herramienta la imagen invierte su color');
    }

    if (tools == "stack") {
      this.disableAllTools();
      cornerstoneTools.stackScrollTouchDrag.activate(this.element);

    }

    if (tools == "reset") {

      this.disableAllTools();
      cornerstone.reset(this.element);
      cornerstone.resize(this.element, false);
      $('#tips').text('Tips: Al hacer click en la herramienta restaura el estado original de la imagen');
    }

    if (tools == "retirarInvertir") {
      this.disableAllTools();
      const viewport = cornerstone.getViewport(this.element);
      viewport.invert = !viewport.invert;
      cornerstone.setViewport(this.element, viewport);
      $('#tips').text('Tips: Al hacer click en la herramienta la imagen invierte su color');
    }

    if (tools == "rotate2") {

      const viewport = cornerstone.getViewport(this.element);
      viewport.rotation -= 90;
      cornerstone.setViewport(this.element, viewport);
    }

  }

  disableAllTools() {

    cornerstoneTools.pan.deactivate(this.element);
    cornerstoneTools.panTouchDrag.deactivate(this.element);
    cornerstoneTools.rotate.deactivate(this.element);
    cornerstoneTools.rotateTouchDrag.deactivate(this.element);
    cornerstoneTools.rotateTouch.disable(this.element);
    // cornerstoneTools.ellipticalRoiTouch.deactivate(this.element);
    // cornerstoneTools.angleTouch.deactivate(this.element);
    // cornerstoneTools.rectangleRoiTouch.deactivate(this.element);
    // cornerstoneTools.lengthTouch.deactivate(this.element);
    // cornerstoneTools.probeTouch.deactivate(this.element);
    cornerstoneTools.zoom.deactivate(this.element);
    cornerstoneTools.zoomTouchDrag.deactivate(this.element);
    cornerstoneTools.zoomTouchPinch.deactivate(this.element);
    cornerstoneTools.wwwc.deactivate(this.element);
    cornerstoneTools.wwwcTouchDrag.deactivate(this.element);
    cornerstoneTools.stackScrollTouchDrag.deactivate(this.element);
    cornerstoneTools.magnify.deactivate(this.element);
    cornerstoneTools.magnifyTouchDrag.deactivate(this.element);
    $("#brillo").removeClass('botonActivado');
    $("#brillospan").removeClass('spanActivado');
    $("#stack").removeClass('botonActivado');
    $("#stackspan").removeClass('spanActivado');
    $('#tips').text("");
  }

}

export const environment = {
  production: true,
  endpoint: {
    log: 'https://v.bioboxcloud.com/int/',
    visor: 'https://p.bioboxcloud.com/',
  },
  login: {
    nroturno: '',
    codigo: '',
  },
  logEnabled: true,
};
/// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  endpoint: {
    // log: 'https://v.bioboxcloud.com/int/',
    // visor: 'http://192.168.100.17:8100/',
    //visor: 'http://'+window.location.hostname+':8100/',
    //visor: 'https://p.bioboxcloud.com/',

    log: 'https://v.bioboxcloud.com/int/',
    visor: 'http://localhost:8100/',


  },
  endpointDev: {
    log: 'http://'+window.location.hostname+':8091/',
    visor: 'http://'+window.location.hostname+':8100/',
  },
  login: {
    nroturno: '403764',
    codigo: 'N8RMPR98',
    // nroturno: '694852',
    // codigo: '72CXYCXU',
  },
  logEnabled: true,


};


/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.

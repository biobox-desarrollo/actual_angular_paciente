#!/bin/bash
rm -rf dist/visor2
ng build --output-hashing=all --build-optimizer --optimization --configuration production
cd dist/visor2
fn=`date | md5sum | cut -d ' ' -f 1`
mv index.html index-${fn}.html
ln -s index-${fn}.html index.html